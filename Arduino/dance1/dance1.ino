
#include <DynamixelShield.h>

#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MEGA2560)
  #include <SoftwareSerial.h>
  SoftwareSerial soft_serial(7, 8); // DYNAMIXELShield UART RX/TX
  #define DEBUG_SERIAL soft_serial
#elif defined(ARDUINO_SAM_DUE) || defined(ARDUINO_SAM_ZERO)
  #define DEBUG_SERIAL SerialUSB    
#else
  #define DEBUG_SERIAL Serial
#endif

const float DXL_PROTOCOL_VERSION = 1.0;

DynamixelShield dxl;

const int joints = 3;

//This namespace is required to use Control table item names
using namespace ControlTableItem;

void setup() {
  // put your setup code here, to run once:
  
  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  DEBUG_SERIAL.begin(115200);

  // Set Port baudrate to 57600bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(1000000);
  //dxl.begin(57600);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  for (int i = 0; i < joints; ++i) {
    // Get DYNAMIXEL information
    dxl.ping(i);
    // Turn off torque when configuring items in EEPROM area
    dxl.torqueOff(i);
    dxl.setOperatingMode(i, OP_POSITION);
    dxl.torqueOn(i);
    dxl.setGoalVelocity(i, 15.0, UNIT_PERCENT);
  }
}

void loop() {
  static int count = 0;
  int id = 1;
  dxl.setGoalVelocity(id, 7.0, UNIT_PERCENT);

  dxl.setGoalPosition(id, 400);
  delay(600);
  id = 2;
  dxl.setGoalVelocity(id, 7.0, UNIT_PERCENT);
  dxl.setGoalPosition(id, 0);
  delay(200);

  id = 3;
  dxl.setGoalVelocity(id, 9.0, UNIT_PERCENT);
  dxl.setGoalPosition(id, count%2 ? 0:1000);
  delay(300);

  id = 1;
  dxl.setGoalVelocity(id, 9.0, UNIT_PERCENT);
  dxl.setGoalPosition(id, 600);
  delay(600);

  id = 2;
  dxl.setGoalVelocity(id, 7.0, UNIT_PERCENT);

  dxl.setGoalPosition(id, 100);
  delay(400);
  count++;
}
