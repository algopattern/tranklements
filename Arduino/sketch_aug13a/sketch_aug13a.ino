
//const int controlPin[16] = {2,3,4,5,6,7,8,9,10,11,12,13,A0,A1,A2,A3}; // define pins

const int controlPin[16] = {11,3,12,4,A0,5,13,6,A1,7,A2,8,A3,9,2,10};

int debug = false;
int maxBits = 16;
int strength = 4;
void setup() {
  for(int i=0; i<16; i++)
  {
    pinMode(controlPin[i], OUTPUT);// set pin as output
    digitalWrite(controlPin[i], LOW); 
  }
  
  Serial.begin(115200);
  Serial.print("hello\n");
}


void pulse_solenoids() {
  static int pulses = 0;

  for (int i=0; i < maxBits; ++i) {
    long int start = millis();
    long int d = 0;
    int pulse = 0;
    while ((millis() - start) < 700) {
      for (int j=0; j < maxBits; ++j) {
        int active = (j == i) || ((j < i) && ((pulse % strength) == (j % strength)));
        digitalWrite(controlPin[j], active ? HIGH : LOW);
      } 
      pulse++;
    }
  }
  for (int j=0; j < maxBits; ++j) {
    digitalWrite(controlPin[j], LOW);
  } 
  //delay(2000);
}

void loop() {
  pulse_solenoids();
}
