#include <Dynamixel2Arduino.h>
#include "Kinematics.h"
#include "MatrixUtils.h"
#include <SoftwareSerial.h>

#define joints 3


/*
     ___
    |o o|
    :___: 
   .--u--.
   |.---.|
   |=   =|
    |   | 
    |___|
    ,| |.
    ||o||     z y
    | - |     |/
     ---      `--x

connectors fp04-f2 / f3
http://en.robotis.com/service/downloadpage.php?ca_id=7040

     L1 = 26 + 5.5 + 36 = 67.5
     L2 = 26 + 40 = 66
     L3 = 36
              
*/

#define L1 67.5
#define L2 66
#define L3 36

SoftwareSerial soft_serial(7, 8);
//#define DEBUG_SERIAL soft_serial  
//#define debug DEBUG_SERIAL.println
#define DEBUG_SERIAL Serial  
#define debug Serial.println
Dynamixel2Arduino dxl(Serial, 2);

void debug_matrix(float* mat, int r, int c, String message) { 
    debug(message);
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < c; j++) {
            debug(mat[c * i + j]);
            debug("\t");
        }
        debug();
    }
    debug();
}


void setup() {
    // Serial.begin(9600);
    DEBUG_SERIAL.begin(9600);
    while(!DEBUG_SERIAL); // wait until open  
    debug("hello from setup()");

    Kinematics kin(joints);
    MatrixUtils mat_utils;

    
//  dxl.begin(1000000);
//  dxl.setPortProtocolVersion(1.0);
//  for (int i = 1; i <=joints; ++i) {
//    dxl.ping(i);
//    dxl.torqueOff(i);
//    dxl.setOperatingMode(i, OP_POSITION);
//    dxl.torqueOn(i);
//    dxl.setGoalVelocity(i, 15.0, UNIT_PERCENT);
//  }

    debug("add joints");

    // aligned with y axis
    // Treat the centre of the first axis as the origin?
    kin.add_joint_axis(0, -1,  0, 0, 0, 0);
    
    // joint aligned with x axis
    // 26 + 42 = 68mm above
    kin.add_joint_axis(-1, 0, 0, 0, -L1, 0);

    // aligned with z axis
    // 68 from previous, plus another 26.. and 20 for middle? = 114mm
    kin.add_joint_axis(0, 0, 1, 0, 0, 0);

    debug("add nose pose");
    // nose points -38mm out

    kin.add_initial_end_effector_pose(1, 0, 0, 0,
                                      0, 1, 0, -L3,
                                      0, 0, 1, L1+L2,
                                      0, 0, 0, 1
                                     );


    float desired_transform[4][4] = {
        {1, 0,  0, 0},
        {0, 1,  0, -L3},
        {0, 0,  1, L1+L2},
        {0, 0,  0, 1}
    };

    float jac[6][joints];
    float jac_t[6][joints];
    float AA_t[6][6];
    float A_tA[joints][joints];
    float pinv[joints][6];

    float joint_angles_0[joints] = {0, 0, 0};

    float joint_angles[joints] = {-1,-1,-1};

    debug("get the transform");
    kin.inverse((float*)desired_transform, (float*)jac, (float*)pinv, (float*)jac_t, (float*)AA_t, 
                (float*)A_tA, joint_angles_0, 0.01, 0.001, 20, joint_angles);
    
    debug("print the matrix");
    debug_matrix(joint_angles, 1, joints, "Joint angles");
}

void loop() {
  // put your main code here, to run repeatedly:

}
